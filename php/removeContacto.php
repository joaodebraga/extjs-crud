<?php
include ("db.php");
$dados = $_POST['contactos'];
$registo = json_decode(stripslashes($dados), true);
// (Delete) curl -X POST --form 'contactos={"id":"11"}' http://localhost/~jgr/ExtJS4FirstLook/Code/Chapter%207/app/php/removeContacto.php
$id = $registo['id'];
$tabela = 'contacto';
$sql = "delete from " . $tabela . " where id = " . $id;
$affected = &$mdb2 -> exec($sql);
if (PEAR::isError($affected)) {
	$result["success"] = false;
	$result["errors"]["reason"] = $affected -> getMessage();
	$result["errors"]["query"] = $sql;
	die(json_encode($result));
} else {
	$linhasAfetadas = $affected;
	$result["total"] = $linhasAfetadas;
	if ($linhasAfetadas > 0) {
		$result["feedback"] = "O contacto '" . $id . "' foi removido.";
	} else {
		$result["feedback"] = "O contacto '" . $id . "' não foi removido.";
	}
	$result["success"] = true;
	$result["sql"] = $sql;
	echo json_encode($result);
}
?>